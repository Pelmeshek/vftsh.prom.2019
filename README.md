# Репозиторий курса Промышленное программирование ВФТШ #
В папке **[seminars](https://bitbucket.org/Pelmeshek/vftsh.prom.2019/src/master/seminars/)** будут выкладываться материалы с занятий  
В папке **[home_tasks](https://bitbucket.org/Pelmeshek/vftsh.prom.2019/src/master/home_tasks/)** будут выкладываться домашние задания  
**[C++ style guide или как писать код:](https://bitbucket.org/Pelmeshek/vftsh.prom.2019/src/master/style_guide.md)**  
**[Табличка с баллами](https://docs.google.com/spreadsheets/d/1pNE-HXGLAuJCWB5SBlCXKbyRTptFG8EGN6WMdLbq1QM/edit#gid=0)**  

**Полезные ссылки**:  
[Миникурс по основам с++](https://stepik.org/course/16480/syllabus)  
[Изучение git в игровой форме](https://learngitbranching.js.org/). Здесь задачи могут попасьтся сложнее требуемого в вфтш уровня, но штука интересная  
