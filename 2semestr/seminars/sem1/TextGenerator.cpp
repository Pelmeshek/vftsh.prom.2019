﻿#define _CRT_SECURE_NO_WARNINGS // это исключительно для того, чтобы все нормально компилилось в вижуалке
#include <cstdio>

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <unordered_map>

#define START "["
#define END "]"

using std::unordered_map;
using std::string;

struct Link {
	unsigned long long Count;
	unordered_map<string, unsigned long> Edges;
};

struct MarcovChain {
	unordered_map<string, Link> Chain;
};

class TekstHandel {
	bool IsCorrectSymbol(char c) {
		//тут должна быть функция, проверяющая, является ли символ буквой, или пробелом, или нет
		return true;
	}

	bool IsEndSymbol(char c) {
		//тут должна быть функция, проверяющая, является ли символ концом прделожени('.', '!' или '?')
		return false;
	}

	char ToLowerCase(char c) {
		//тут должна быть функция, переводящая буквы в нижний регистр
		return c;
	}

	void ParseSentence(MarcovChain& MC, string s) {
		//добавляем звенья в цепь маркова из предложения  s
		//не забываем про искусственные стартовое и конечное состояние, можно использовать задефайненные 
	}
public:
	MarcovChain	Handler(string FileName) {
		MarcovChain MC;
		FILE* f = fopen(FileName.c_str(), "r");
		char c;
		c = fgetc(f);
		string buf = "";
		while (c != EOF) {

			c = fgetc(f);
			if (IsCorrectSymbol(c)) {
				buf += ToLowerCase(c);
			} 
			else if (IsEndSymbol(c)) {
				ParseSentence(MC, buf);
			}
		}
		ParseSentence(MC, buf);
		return MC;
	}
};

class TextGenerator {
	//тут нужно сделать метод, генерирующую предложение по построенной цепи маркова
};

