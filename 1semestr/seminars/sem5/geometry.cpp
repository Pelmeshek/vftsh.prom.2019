#include <iostream>
#include <cmath>

class IFigure {
public:
    virtual double Square() = 0;

    virtual double Perimeter() = 0;
};

class TPoint {
public:
    int x;
    int y;

    TPoint(int x_ = 0, int y_ = 0) : x(x_), y(y_) {}

    double Length(TPoint OtherPoint) {
        return sqrt((x - OtherPoint.x) * (x - OtherPoint.x) +
                    (y - OtherPoint.y) * (y - OtherPoint.y));
    }
};

class TQuadrangle : public IFigure {
public:
    TQuadrangle() {}

    TQuadrangle(TPoint a_, TPoint b_, TPoint c_, TPoint d_) : a(a_), b(b_), c(c_), d(d_) {}

    TPoint a, b, c, d;

    double Perimeter() {
        return a.Length(b) + a.Length(d) + c.Length(d) + b.Length(c);
    }
};

class TRectangle : public TQuadrangle {
public:
    TRectangle(TPoint a_, TPoint b_, TPoint c_, TPoint d_) : TRectangle::TQuadrangle(a_, b_, c_, d_) {}

    double Square() {
        return a.Length(b) * a.Length(d);
    }
};

class TTrapezoid : public TQuadrangle {
public:
    double Square() {
        return ((a.Length(d) + b.Length(c)) / 2) * (a.y - b.y);
    }
};

int main() {
    TPoint a(0,0),
            b(0,3),
            c(3,3),
            d(3,0);
    TRectangle rectangle(a, b, c, d);
    std::cout << rectangle.Square() << " square" << std::endl;
    std::cout << rectangle.Perimeter() << " perimeter" << std::endl;

}

