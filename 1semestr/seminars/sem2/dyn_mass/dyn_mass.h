#ifndef DYNMASS_H
#define DYNMASS_H

class DynMass {
public:
    DynMass();
    DynMass(int fullCount);
    void PushBack(int a);
    int operator[](int index);
    void Clear();

private:
    int RealCount = 0;
    int FullCount;
    int* Array;
};

#endif //DYNMASS_H
