#include "DynMass.h"

DynMass::DynMass() {
    FullCount = 10;
    Array = new int[FullCount];
}

DynMass::DynMass(int fullCount) {
    FullCount = fullCount;
    Array = new int[FullCount];
}

void DynMass::PushBack(int a) {
    if (RealCount < FullCount)
    {
        Array[RealCount] = a;
        RealCount++;
    }
    else
    {
        FullCount *= 2;
        int* newArray = new int[FullCount];
        for(int i = 0; i < RealCount; i++)
            newArray[i] = Array[i];
        newArray[RealCount] = a;
        delete[] Array;
        Array = newArray;
        RealCount++;
    }
}

int DynMass::operator[](int index) {
    return Array[index];
}

void DynMass::Clear() {
    delete[] Array;
    RealCount = 0;
    FullCount = 0;
}


