#include "DynMass.h"
#include <iostream>

int main() {
    DynMass d;
    d.PushBack(1);
    assert(d[0] == 1);
    d.PushBack(2);
    d.PushBack(3);
    assert(d[1] == 2);
    assert(d[2] == 3);
    return 0;
}