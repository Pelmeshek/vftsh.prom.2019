#include <iostream>

class DynMass{
    int RealCount = 0;
    int FullCount;
    int* Array;
    DynMass(int fullCount = 10){
        FullCount = fullCount;
        Array = new int[FullCount];
    }
    void PushBack(int a)
    {
        if (RealCount < FullCount)
        {
            Array[RealCount] = a;
            RealCount++;
        }
        else
        {
            FullCount *= 2;
            int* newArray = new int[FullCount];
            for(int i = 0; i < RealCount; i++)
                newArray[i] = Array[i];
            newArray[RealCount] = a;
            delete[] Array;
            Array = newArray;
            RealCount++;
        }
    }
    int operator[](int index){
        return Array[index];
    }
    void Clear(){
        delete[] Array;
        RealCount = 0;
        FullCount = 0;
    }
};
