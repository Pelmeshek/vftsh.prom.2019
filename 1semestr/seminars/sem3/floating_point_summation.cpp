#include <iostream>
#include <iomanip>

int main() {
   std::cout << std::setprecision(20);

    double sum1 = 0;

    for (int i = 1; i <= 100000; ++i) {
        sum1 += 1.0 / i;
    }

    std::cout << sum1 << " sum1\n";

    double sum2 = 0;

    for (int i = 100000; i >= 1; --i) {
        sum2 += 1.0 / i;
    }

    std::cout << sum2 << " sum2\n";

    return 0;
}